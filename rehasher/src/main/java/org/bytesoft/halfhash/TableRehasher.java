package org.bytesoft.halfhash;


import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.HFileOutputFormat;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.bytesoft.hbaseutils.BulkLoadHelper;
import org.bytesoft.utils.StringHasher;

import java.io.IOException;
import java.util.NavigableMap;

/*
 * convert table to half-hashed variant
 */
public class TableRehasher extends Configured implements Tool {
    static private final String REHASHER_NBITS = "rehasher.nbits";

    static public void main(String args[]) throws Exception {
        int rc = ToolRunner.run(HBaseConfiguration.create(), new TableRehasher(), args);
        System.exit(rc);
    }

    private void PrintUsage() {
        System.err.printf("Usage: %s src_dir dst_table nbits\n", TableRehasher.class.getCanonicalName());
    }

    @Override
    public int run(String[] args) throws Exception {
        if (args.length != 3) {
            PrintUsage();
            return 64;
        }

        String src_dir = args[0];
        String dst_table = args[1];
        int nbits = Integer.valueOf(args[2]);

        BulkLoadHelper load_helper = new BulkLoadHelper(getConf());

        Job job = MakeJobConf(src_dir, dst_table, nbits, load_helper.GetBulksDirectory());
        if (!job.waitForCompletion(true))
            return 1;

        load_helper.BulkLoad(dst_table);
        return 0;
    }

    private Job MakeJobConf(String src_path, String dst_table, int nbits, Path bulks_dir) throws IOException {
        getConf().setInt(REHASHER_NBITS, nbits);
        Job job = Job.getInstance(getConf(), TableRehasher.class.getCanonicalName());
        job.setJarByClass(TableRehasher.class);

        job.setInputFormatClass(SequenceFileInputFormat.class);
        SequenceFileInputFormat.addInputPath(job, new Path(src_path));

        job.setMapperClass(RehasherMapper.class);
        job.setMapOutputKeyClass(ImmutableBytesWritable.class);
        job.setMapOutputValueClass(Put.class);

        HFileOutputFormat.setOutputPath(job, bulks_dir);
        HFileOutputFormat.configureIncrementalLoad(job, new HTable(getConf(), dst_table));

        return job;
    }

    static Put ConvertResult2Put(byte[] row, Result r) throws IOException {
        Put put = new Put(row);

        NavigableMap<byte[], NavigableMap<byte[], NavigableMap<Long, byte[]>>> map = r.getMap();

        for (byte[] family: map.keySet()) {
            NavigableMap<byte[], NavigableMap<Long, byte[]>> family_content = map.get(family);
            for (byte[] qualifier: family_content.keySet()) {
                NavigableMap<Long, byte[]> qualifier_content = family_content.get(qualifier);
                for (Long ts: qualifier_content.keySet()) {
                    put.add(family, qualifier, ts, qualifier_content.get(ts));
                }

            }
        }

        return put;
    }

    static public class RehasherMapper extends Mapper<ImmutableBytesWritable, Result, ImmutableBytesWritable, Put> {
        int nbits_hash;

        @Override
        protected void setup(Context context) throws IOException, InterruptedException {
            super.setup(context);
            nbits_hash = context.getConfiguration().getInt(REHASHER_NBITS, 8);
        }

        @Override
        protected void map(ImmutableBytesWritable key, Result value, Context context) throws IOException, InterruptedException {
            final byte[] new_key = StringHasher.Hash(key.get(), nbits_hash);
            Put put = ConvertResult2Put(new_key, value);
            context.write(new ImmutableBytesWritable(new_key), put);
        }
    }
}