package org.bytesoft.halfhash;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.Result;
import org.apache.hadoop.hbase.client.Scan;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.TableMapReduceUtil;
import org.apache.hadoop.hbase.mapreduce.TableMapper;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.compress.Lz4Codec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;

import java.io.IOException;

/**
 * Dump HBase table to read quickly later (from HDFS)
 * Dump as SequenceFiles to make compact and splittable
 */
public class TableDumper extends Configured implements Tool {
    private void PrintUsage() {
        System.err.printf("Usage: %s src_table path\n", TableDumper.class.getCanonicalName());
    }

    private void applyRanges(Scan scan) {
        String start_row = getConf().get("dumper.start_row");
        if (start_row != null)
            scan.setStartRow(start_row.getBytes());


        String stop_row = getConf().get("dumper.stop_row");
        if (stop_row != null)
            scan.setStopRow(stop_row.getBytes());
    }

    @Override
    public int run(String[] args) throws Exception {
        if (args.length != 2) {
            PrintUsage();
            return 64;
        }

        String src_table = args[0];
        String dst_dir = args[1];

        Job job = MakeJobConf(src_table, dst_dir);
        return (job.waitForCompletion(true)) ? 0 : 1;
    }

    private Job MakeJobConf(String src_table, String dst_dir) throws IOException {
        Job job = Job.getInstance(getConf(), TableDumper.class.getCanonicalName());
        job.setJarByClass(TableDumper.class);

        Scan scan = new Scan();
        applyRanges(scan);

        TableMapReduceUtil.initTableMapperJob(
                src_table,
                scan,
                DumperMapper.class,
                ImmutableBytesWritable.class, Result.class,
                job);

        job.setOutputKeyClass(ImmutableBytesWritable.class);
        job.setOutputValueClass(Result.class);

        job.setOutputFormatClass(SequenceFileOutputFormat.class);
        SequenceFileOutputFormat.setOutputPath(job, new Path(dst_dir));
        SequenceFileOutputFormat.setOutputCompressionType(job, SequenceFile.CompressionType.BLOCK);
        SequenceFileOutputFormat.setOutputCompressorClass(job, Lz4Codec.class);

        // TODO: maybe we should set block size too?

        job.setNumReduceTasks(0);

        return job;
    }

    static public class DumperMapper extends TableMapper<ImmutableBytesWritable, Result> {
        @Override
        protected void map(ImmutableBytesWritable key, Result value, Context context) throws IOException, InterruptedException {
            context.write(key, value);
        }
    }

    static public void main(String args[]) throws Exception {
        int rc = ToolRunner.run(HBaseConfiguration.create(), new TableDumper(), args);
        System.exit(rc);
    }
}
