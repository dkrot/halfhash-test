package org.bytesoft.halfhash;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.client.HTable;
import org.apache.hadoop.hbase.client.Put;
import org.apache.hadoop.hbase.io.ImmutableBytesWritable;
import org.apache.hadoop.hbase.mapreduce.HFileOutputFormat;
import org.apache.hadoop.hbase.util.Bytes;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.bytesoft.hbaseutils.BulkLoadHelper;

import java.io.IOException;


public class InitialTableMaker extends Configured implements Tool {
    public static void main(String[] args) throws Exception {
        int rc = ToolRunner.run(HBaseConfiguration.create(), new InitialTableMaker(), args);
        System.exit(rc);
    }

    @Override
    public int run(String[] args) throws Exception {
        String input_path = args[0];
        String output_table = args[1];
        BulkLoadHelper load_helper = new BulkLoadHelper(getConf());

        Job job = MakeJobConf(input_path, output_table, load_helper.GetBulksDirectory());
        if (!job.waitForCompletion(true))
            return 1;

        load_helper.BulkLoad(output_table);
        return 0;
    }

    private Job MakeJobConf(String input_path, String output_table, Path bulks_dir) throws IOException {
        Job job = Job.getInstance(getConf(), InitialTableMaker.class.getName());
        job.setJarByClass(InitialTableMaker.class);
        FileInputFormat.addInputPath(job, new Path(input_path));

        job.setMapperClass(TextInputMapper.class);
        job.setMapOutputKeyClass(ImmutableBytesWritable.class);
        job.setMapOutputValueClass(Put.class);

        HFileOutputFormat.setOutputPath(job, bulks_dir);
        HFileOutputFormat.configureIncrementalLoad(job, new HTable(getConf(), output_table));

        return job;
    }

    static public class TextInputMapper extends Mapper<LongWritable, Text, ImmutableBytesWritable, Put> {
        @Override
        protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
            String url = value.toString();

            Put put = new Put(url.getBytes());
            put.add(Bytes.toBytes("flg"), Bytes.toBytes("w"), Bytes.toBytes("Y"));
            put.add(Bytes.toBytes("flg"), Bytes.toBytes("owner"), Bytes.toBytes("mail"));

            context.write(new ImmutableBytesWritable(url.getBytes()), put);
        }
    }
}