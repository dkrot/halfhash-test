import static org.junit.Assert.*;
import org.junit.Test;
import org.bytesoft.utils.StringHasher;

import java.util.HashSet;


public class TestHasher {
    private String extract_hash(String url_with_prefix) {
        int index = url_with_prefix.indexOf("http");

        assertNotEquals(-1, index);
        return url_with_prefix.substring(0, index);
    }

    private String str_hash(String str, int nbits) {
        return new String(StringHasher.Hash(str.getBytes(), nbits));
    }

    private final static byte[] hexBytes = "0123456789abcdef".getBytes();
    private final String big_url = "http://banki.ru/sitemap/debetovaya_karta_mmcis_v_gorodah/Kaliningradskaya_oblast~/Gur~evsk/Samara/Altayskiy_kray/Polovinka";

    private byte[] ord_array(byte[] bytes) {
        byte[] res = new byte[bytes.length];
        for (int i = 0; i < bytes.length; i++)
            res[i] = hexBytes[bytes[i]];

        return res;
    }

    @Test
    public void testSet1() {
        HashSet<String> values = new HashSet<String>();

        for (int i = 0; i < 10000; i++) {
            String url = String.format("http://youtube.com/%d.html", i);
            String h = extract_hash(str_hash(url, 8));

            assertEquals("length of 8 bits in hex representation is 2!", 2, h.length());
            assertTrue(h.compareTo("00") >= 0);
            assertTrue(h.compareTo("ff") <= 0);

            values.add(h);
        }

        assertEquals("All values expected to be met",256, values.size());
    }

    @Test
    public void testPerformance() {
        byte[] s = big_url.getBytes();

        // it takes ~ 950ms on Intel(R) Core(TM) i7-4771 CPU @ 3.50GHz
        for (int i = 0; i < 10000000; i++) {
            StringHasher.Hash(s, 9);
        }
    }

    @Test
    public void testNoHash() {
        String url = "http://banki.ru/sitemap/debetovaya_karta_mmcis_v_gorodah/Kaliningradskaya_oblast~/Gur~evsk/Samara/Altayskiy_kray/Polovinka";
        assertEquals(url, str_hash(big_url, 0));
    }

    @Test
    public void test1Bit() {
        HashSet<String> values = new HashSet<String>();

        for (int i = 0; i < 100; i++) {
            String url = String.format("http://youtube.com/%d.html", i);
            String h = extract_hash(str_hash(url, 1));

            values.add(h);
        }

        assertEquals("There should be only 2 different values with 1 bit",2, values.size());
        assertTrue(values.contains("0"));
        assertFalse("split should be by _higher_ bit, not lower", values.contains("1"));
        assertTrue("split should be by _higher_ bit, not lower", values.contains("8"));
    }

    @Test
    public void test9Bits() {
        HashSet<String> values = new HashSet<String>();

        for (int i = 0; i < 10000; i++) {
            String url = String.format("http://youtube.com/%d.html", i);
            String h = extract_hash(str_hash(url, 9));

            values.add(h);
        }

        byte[] bytes = new byte[3];

        for (bytes[0] = 0; bytes[0] <= 0xf; bytes[0]++)
            for (bytes[1] = 0; bytes[1] <= 0xf; bytes[1]++)
                for (bytes[2] = 0; bytes[2] <= 8; bytes[2] += 8) {
                    String combination = new String(ord_array(bytes));
                    assertTrue("'" + combination + "' should be met", values.contains(combination));
                }

        assertEquals("Should be exactly 2**9 variants", 512, values.size());
    }
}
