package org.bytesoft.utils;
import ie.ucd.murmur.MurmurHash;

public class StringHasher {
    private final static byte[] hexBytes = "0123456789abcdef".getBytes();

    public static byte[] Hash(byte[] str, int nbits) {
        if (nbits == 0)
            return str;

        int nhex_bytes = (nbits - 1) / 4 + 1;
        // we should have '0' or '8' for nbits=1 (or '000'...'ff8' for nbits=9)
        int padding_shift = (4 - (nbits & 3)) & 3;
        byte[] res = new byte[str.length + nhex_bytes];

        int h = Math.abs(MurmurHash.hash32(str, str.length)) << padding_shift;
        for (int i = nhex_bytes - 1; i >= 0; i--) {
            res[i] = hexBytes[h & 0xf];
            h >>= 4;
        }

        System.arraycopy(str, 0, res, nhex_bytes, str.length);
        return res;
    }
}
