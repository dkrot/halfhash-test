#!/usr/bin/bash
# rehash firstmeet table by special hash type

NBITS=9

# it's better to take single compacted region to compare
START_ROW="http://banki.ru/sitemap/debetovaya_karta_mmcis_v_gorodah/Kaliningradskaya_oblast~/Gur~evsk/Samara/Altayskiy_kray/Polovinka"
STOP_ROW="http://banki.ru/sitemap/debetovaya_karta_mmcis_v_gorodah/Nizhniy_Novgorod/Altayskiy_kray/Sokolovo_(Zonal~nyiy_r-n)/Moskva_i_oblast~/Razvilka"

hadoop jar ./rehasher/build/libs/rehasher.jar org.bytesoft.halfhash.TableDumper -Dhbase.client.scanner.caching=100000 \
        -D"rehasher.start_row=$START_ROW" -D"rehasher.stop_row=$STOP_ROW" \
        firstmeet firstmeet_hash $NBITS
